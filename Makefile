DOCKER_COMPOSE = docker-compose
NODE 		   = ${DOCKER_COMPOSE} run --rm node

start:
	${DOCKER_COMPOSE} up -d

stop:
	${DOCKER_COMPOSE} down

test:
	$(NODE) node main.js

restart: stop start

kill:
	$(DOCKER_COMPOSE) kill
	$(DOCKER_COMPOSE) down --volumes --remove-orphans

clean: kill
	rm -rf node_modules

reset: clean 

node:
	$(NODE) bash

.PHONY: start stop restart kill clean reset node
